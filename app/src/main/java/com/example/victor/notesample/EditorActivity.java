package com.example.victor.notesample;

import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class EditorActivity extends AppCompatActivity {

    public static final String INTENT_KEY_NOTE_ID = "note_id";
    private DBHelper mDBHelper;
    private Note mNote;

    private EditText mETTitle, mETDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        mETDescription = (EditText) findViewById(R.id.tv_note_text);
        mETTitle = (EditText) findViewById(R.id.tv_title);

        mDBHelper = new DBHelper(this);
        int id = getIntent().getIntExtra(INTENT_KEY_NOTE_ID, -1);


    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Новая заметка");
            setTitle("Новая заметка");
        }
    }
}
