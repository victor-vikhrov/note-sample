package com.example.victor.notesample;

/**
 * Created by victor on 17.02.17.
 */
public class Note {

    private int id;
    private String title;
    private String description;
    private Long date;

    public Note(String title, Long date) {
        this.title = title;
        this.date = date;
    }

    public Note() {

    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
